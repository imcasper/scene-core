package casperix.scene.camera.orbital

import casperix.math.spherical.float64.SphericalCoordinateDouble
import casperix.math.vector.float64.Vector2d
import casperix.math.vector.float64.Vector3d
import casperix.misc.clamp
import casperix.misc.max
import casperix.misc.min
import kotlin.math.PI

class OrbitalCameraTransformer(settings: OrbitalCameraTransformSettings, val outputState: (OrbitalCameraState) -> Unit) {
    private var offset = SphericalCoordinateDouble(100.0, PI / 4.0, 0.0)
    private var pivot = Vector2d.ZERO

    var settings = settings
        set(value) {
            field = value
            setPivot(pivot)
            setOffset(offset)
        }

    init {
        setPivot(pivot)
        setOffset(offset)
    }

    fun getPosition(): Vector3d {
        return pivot.expand(0.0) + getOffset().fromSpherical()
    }

    fun getOffset(): SphericalCoordinateDouble {
        return offset
    }

    fun getPivot(): Vector2d {
        return pivot
    }

    fun setOffset(value: SphericalCoordinateDouble) {
        val verticalAngle = value.verticalAngle.clamp(settings.minVerticalAngle, settings.maxVerticalAngle)
        val horizontalAngle = clampIfValid(value.horizontalAngle, settings.minHorizontalAngle, settings.maxHorizontalAngle)

        offset = SphericalCoordinateDouble(value.range.clamp(settings.minRange, settings.maxRange), verticalAngle, horizontalAngle)
        dispatchState()
    }

    private fun clampIfValid(value: Double, minValue: Double?, maxValue: Double?): Double {
        var result = value
        if (minValue != null && !minValue.isNaN()) result = max(result, minValue)
        if (maxValue != null && !maxValue.isNaN()) result = min(result, maxValue)
        return result
    }

    fun setPivot(value: Vector2d) {
        pivot = value.clamp(settings.pivotBox.min, settings.pivotBox.max)
        dispatchState()
    }

    fun zoom(value: Double) {
        setOffset(SphericalCoordinateDouble(offset.range + value, offset.verticalAngle, offset.horizontalAngle))
    }

    fun translate(right: Double, forward: Double) {
        val rightSpherical = offset.run { copy(horizontalAngle = horizontalAngle + PI / 2) }
        val rightProjection = rightSpherical.fromSpherical().getXY().normalize()
        val forwardProjection = -offset.fromSpherical().getXY().normalize()
        val delta = rightProjection * right + forwardProjection * forward

        setPivot(pivot + delta)
    }

    fun rotate(deltaHorizontal: Double, pitch: Double) {
        setOffset(SphericalCoordinateDouble(offset.range, offset.verticalAngle + pitch, offset.horizontalAngle + deltaHorizontal))
    }

    private fun dispatchState() {
        outputState(OrbitalCameraState(getPivot(), getOffset()))
    }

}