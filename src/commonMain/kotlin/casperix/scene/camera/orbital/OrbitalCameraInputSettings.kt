package casperix.scene.camera.orbital

import casperix.input.KeyButton
import casperix.input.PointerButton
import kotlin.math.PI

enum class TranslateDepends {
	NOTHING,
	RANGE,
	HEIGHT,
}
/**
 *
 * 	Translate -- is move in concrete direction with fixed speed . Direction is up, down, left or right
 *
 * 	Drag -- shifting the camera just like the pointer
 *
 */
data class OrbitalCameraInputSettings(
	val translateDependsOnHeight: TranslateDepends = TranslateDepends.RANGE,
	val pointer: OrbitalCameraPointerInputSettings = OrbitalCameraPointerInputSettings(),
	val keyboard: OrbitalCameraKeyboardInputSettings = OrbitalCameraKeyboardInputSettings()
)

/**
 * @param rotateHorizontalAngleFactor    --	change horizontal angle from pointer  by one pixel mul pixelFactor
 * @param rotateVerticalAngleFactor    --	change vertical angle from pointer  by one pixel mul pixelFactor
 * @param zoomStep    --	change zoom for one step (one wheel step may be)
 * @param translateFactor    --	change pivot coordinate from pointer  by one pixel mul pixelFactor
 * @param pixelFactor
 */
data class OrbitalCameraPointerInputSettings(
	val rotateKeys: Set<PointerButton> = setOf(PointerButton.MIDDLE),
	val rotateHorizontalAngleFactor: Double = PI / 5.0,
	val rotateVerticalAngleFactor: Double = PI / 5.0,
	val zoomStep: Double? = 30.0,

	val translateKeys: Set<PointerButton> = setOf(PointerButton.RIGHT),
	val translateFactor: Double = 2.0,

	val pixelFactor:Double = 0.001,
)

/**
 * @param translateSpeed    --	change pivot coordinate from keyboard per one second
 */

data class OrbitalCameraKeyboardInputSettings(
	val rotateVerticalAngleIncreaseKeys: Set<KeyButton> = setOf(KeyButton.R),
	val rotateVerticalAngleDecreaseKeys: Set<KeyButton> = setOf(KeyButton.F),
	val rotateVerticalSpeed: Double = PI,

	val rotateHorizontalAngleIncreaseKeys: Set<KeyButton> = setOf(KeyButton.Q),
	val rotateHorizontalAngleDecreaseKeys: Set<KeyButton> = setOf(KeyButton.E),
	val rotateHorizontalSpeed: Double = PI ,

	val zoomIncreaseKeys: Set<KeyButton> = setOf(KeyButton.Z),
	val zoomDecreaseKeys: Set<KeyButton> = setOf(KeyButton.X),
	val zoomSpeed: Double = 10.0,

	val translateUpKeys: Set<KeyButton> = setOf(KeyButton.ARROW_UP, KeyButton.W),
	val translateDownKeys: Set<KeyButton> = setOf(KeyButton.ARROW_DOWN, KeyButton.S),
	val translateRightKeys: Set<KeyButton> = setOf(KeyButton.ARROW_RIGHT, KeyButton.D),
	val translateLeftKeys: Set<KeyButton> = setOf(KeyButton.ARROW_LEFT, KeyButton.A),
	val translateSpeed: Double = 2.0,
)