package casperix.scene.camera.orbital

import casperix.math.axis_aligned.float64.Box2d
import casperix.math.geometry.fPI
import casperix.math.vector.float64.Vector2d
import casperix.math.vector.float64.Vector3d


data class OrbitalCameraTransformSettings(
	val minVerticalAngle: Double = 0.05,
	val maxVerticalAngle: Double = 0.95 * fPI,
	val minHorizontalAngle:Double? = null,
	val maxHorizontalAngle:Double? = null,
	val minRange: Double = 1.0,
	val maxRange: Double = 1000.0,
	val pivotBox: Box2d = Box2d(Vector2d(-Double.MAX_VALUE), Vector2d(Double.MAX_VALUE)),
	val plainNormal: Vector3d = Vector3d.Z
) {
	init {
		if (minVerticalAngle > maxVerticalAngle) {
			throw Error("Invalid angles: minVerticalAngle=$minVerticalAngle must smaller than maxVerticalAngle=$maxVerticalAngle")
		}
		if (minHorizontalAngle != null && maxHorizontalAngle != null && minHorizontalAngle > maxHorizontalAngle) {
			throw Error("Invalid angles: minHorizontalAngle=$minHorizontalAngle must smaller than maxHorizontalAngle=$maxHorizontalAngle")
		}
		if (minRange > maxRange) {
			throw Error("Invalid angles: minRange=$minRange must smaller than maxRange=$maxRange")
		}
	}
}