package casperix.scene.camera.orbital

import casperix.math.spherical.float64.SphericalCoordinateDouble
import casperix.math.vector.float64.Vector2d
import kotlin.math.PI

data class OrbitalCameraState(
	val pivot: Vector2d,
	val offset: SphericalCoordinateDouble,
) {
	val position get() = pivot.expand(0.0) + offset.fromSpherical()
	val direction get() = -offset.fromSpherical().normalize()
	val up get() = offset.copy(verticalAngle = offset.verticalAngle - PI / 2).fromSpherical().normalize()
}